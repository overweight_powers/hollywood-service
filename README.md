# Hollywood - The Service

Provide metrics against a movies dataset.

## Problem Statement Docs

* https://github.com/GuildEducationInc/backend-engineer-project/blob/main/README.md
* Backend_Engineering_Api.pdf
## Pre-Requisites for running the App
* JDK 16
* Maven


## Running the app
* Navigate to the root of the project (Same directory as the pom.xml)
* Run the command mvn spring-boot:run
* Alternatively you can setup a SpringBoot configuration in your IDE to start the app


## Swagger - API Docs
* Once the app is deployed locally. 
  * Navigate to http://localhost:8080/api/swagger for the API docs

-------------------------------------------App Layers---------------------------------------------
### CONTROLLER LAYER
* Entryway endpoints for the application. Keep it simple, this layer is purely for exposing the endpoint routes
  and swagger information.
    * This directory will also contain object definitions for:
        * requests
        * responses
    
### API LAYER
* This layer is used almost as a documentation layer for the API and will interact with the various servicing layers.
* Response objects are constructed here using data from the service layer

### SERVICE LAYER
* Where the magic happens ^_^

-------------------------------------------The Data Set-------------------------------------------
##This dataset consists of the following files:

**movies_metadata.csv:**
* The main Movies Metadata file. Contains information on 45,000 movies featured in the Full MovieLens dataset. 
Features include posters, backdrops, budget, revenue, release dates, languages, production countries and companies.

**keywords.csv:** 
* Contains the movie plot keywords for our MovieLens movies. 

**credits.csv:** 
* Consists of Cast and Crew Information for all our movies.

**links.csv:** 
* The file that contains the TMDB and IMDB IDs of all the movies featured in the Full MovieLens dataset.

**links_small.csv:** 
* Contains the TMDB and IMDB IDs of a small subset of 9,000 movies of the Full Dataset.

**ratings_small.csv:** 
* The subset of 100,000 ratings from 700 users on 9,000 movies.

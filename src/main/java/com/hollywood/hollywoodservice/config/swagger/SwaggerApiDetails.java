package com.hollywood.hollywoodservice.config.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerApiDetails {

    @Bean
    public OpenAPI springShopOpenAPI() {
        Contact contact = new Contact();
        contact.setEmail("DillonPowers24@gmail.com");
        contact.setName("Dillon Powers");
        return new OpenAPI()
                .info(new Info().title("Hollywood Service API")
                        .description("The API's below are for fetching metrics against a movie dataset")
                        .contact(contact)
                        .version("v1"));
    }
}

package com.hollywood.hollywoodservice.config.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class HollywoodAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {HollywoodException.class})
    public ResponseEntity handleHollywoodException(final HollywoodException e) {
        this.logger.error("Hollywood App Exception thrown with message:" + e.getErrorMessage());
        if(e.getExceptionSource().isPresent()){
            this.logger.error("Exception source message:" + e.getExceptionSource().get().getMessage());
        }
        String error = e.getExceptionSource().isPresent() ? e.getExceptionSource().get().getMessage() : "^_^";
        ApiError apiError =
                new ApiError(e.getStatus(), e.getErrorMessage(), error);
        return new ResponseEntity<Object>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }


}

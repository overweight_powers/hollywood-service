package com.hollywood.hollywoodservice.config.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Optional;

@Data
public class HollywoodException extends RuntimeException {
    private HttpStatus status;
    private String errorMessage;
    private Optional<Throwable> exceptionSource;

    public HollywoodException(HttpStatus status, String errorMessage, Throwable exceptionSource) {
        super(errorMessage, exceptionSource);
        this.status = status;
        this.errorMessage = errorMessage;
        this.exceptionSource = Optional.ofNullable(exceptionSource);
    }

    public HollywoodException(HttpStatus status, String errorMessage) {
        super(errorMessage);
        this.status = status;
        this.errorMessage = errorMessage;
        this.exceptionSource = Optional.empty();
    }
}

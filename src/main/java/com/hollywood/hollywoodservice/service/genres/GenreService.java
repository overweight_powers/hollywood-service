package com.hollywood.hollywoodservice.service.genres;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hollywood.hollywoodservice.service.csv.CsvService;
import com.hollywood.hollywoodservice.service.csv.models.Genre;
import com.hollywood.hollywoodservice.service.csv.models.MovieMetadata;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@Service
public class GenreService {

    private final CsvService csvService;

    private final Type genreListType = new TypeToken<List<Genre>>() {
    }.getType();

    private final Gson gson = new Gson();

    @Autowired
    public GenreService(CsvService csvService) {
        this.csvService = csvService;
    }

    public List<MovieMetadata> getPopularMovieData(int year) {

        Stream<MovieMetadata> metadataStream = csvService.getMovieDataStream(CsvService.MOVIES_METADATA_CSV, MovieMetadata.class);

        return metadataStream
                .filter(metadata -> (metadata.getRelease_date() != null) && metadata.getRelease_date().getYear() == year)
                .sorted((metaDataOne, metaDataTwo) -> Double.compare(metaDataTwo.getRevenue(), metaDataOne.getRevenue()))
                .limit(20)
                .toList();
    }

    public Map<String, Long> getGenreCount(List<MovieMetadata> sourceData) {
        List<Genre> flatGenreList = new ArrayList<>();
        sourceData.stream()
                .map(MovieMetadata::getGenres)
                .map(genreString -> {
                    flatGenreList.addAll(gson.fromJson(genreString, genreListType));
                    return null;
                })
                .toList();

        return flatGenreList.stream()
                .collect(groupingBy(Genre::getName, counting()));
    }
}

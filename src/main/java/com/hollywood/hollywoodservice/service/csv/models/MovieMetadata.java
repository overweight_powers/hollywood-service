package com.hollywood.hollywoodservice.service.csv.models;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@NoArgsConstructor
public class MovieMetadata {

    @CsvBindByName
    private boolean adult;

    @CsvBindByName
    private String belongs_to_collection;

    @CsvBindByName
    private Long budget;

    @CsvBindByName
    private String genres;

    @CsvBindByName
    private String homepage;

    @CsvBindByName
    private int id;

    @CsvBindByName
    private String imdb_id;

    @CsvBindByName
    private String original_language;

    @CsvBindByName
    private String original_title;

    @CsvBindByName
    private String overview;

    @CsvBindByName
    private double popularity;

    @CsvBindByName
    private String poster_path;

    @CsvBindByName
    private String production_companies;

    @CsvBindByName
    private String production_countries;

    @CsvBindByName
    @CsvDate("yyyy-MM-dd")
    private LocalDate release_date;

    @CsvBindByName
    private Long revenue;

    @CsvBindByName
    private float runtime;

    @CsvBindByName
    private String spoken_languages;

    @CsvBindByName
    private String status;

    @CsvBindByName
    private String tagline;

    @CsvBindByName
    private String title;

    @CsvBindByName
    private boolean video;

    @CsvBindByName
    private float vote_average;

    @CsvBindByName
    private int vote_count;
}


//-----NESTED OBJECTS-------
//record MovieCollection (int id, String name, String poster_path, String backdrop_path ) {}
//record ProductionCompany (String name, int id) {}
//record ProductionCountry (String iso_3166_1, String name) {}
//record SpokenLanguage (String iso_639_1, String name) {}

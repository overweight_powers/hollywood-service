//package com.hollywood.hollywoodservice.service.csv.converters;
//
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import com.hollywood.hollywoodservice.service.csv.models.Genre;
//import com.opencsv.bean.AbstractCsvConverter;
//import lombok.extern.slf4j.Slf4j;
//import net.minidev.json.JSONArray;
//
//import java.lang.reflect.Type;
//import java.util.ArrayList;
//import java.util.List;
//
//@Slf4j
//public class ConvertToGenre extends AbstractCsvConverter {
//
//    @Override
//    public Genre convertToRead(String value) {
//        try {
//            value.trim();
//            List<Genre> genres;
//            Type listType = new TypeToken<List<Genre>>() {
//            }.getType();
//            genres = new Gson().fromJson(value, listType);
//            return genres;
//        }
//        catch(Exception e){
//            log.error("Failed printing value: " + value);
//        }
//
//        return new ArrayList<Genre>();
//
//    }
//
//    @Override
//    public String convertToWrite(Object value) {
//        JSONArray t = (JSONArray) value;
//        return String.format(t.toJSONString());
//    }
//}

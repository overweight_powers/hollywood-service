package com.hollywood.hollywoodservice.service.csv.models;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Credits {

    @CsvBindByName
    private String cast;

    @CsvBindByName
    private String crew;

    @CsvBindByName
    private int id;
}

//-----NESTED OBJECTS-------
//record CastMember (int cast_id, String character, String credit_id, int gender, int id, String name, int order, String profile_path) {}
//record CrewMember (String credit_id, String department, int gender, int id, String job, String name, String profile_path) {}

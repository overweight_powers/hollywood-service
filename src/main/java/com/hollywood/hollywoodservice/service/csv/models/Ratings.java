package com.hollywood.hollywoodservice.service.csv.models;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Ratings {

    @CsvBindByName
    private int userId;

    @CsvBindByName
    private double movieId;

    @CsvBindByName
    private double rating;

    @CsvBindByName
    private Long timestamp;

}

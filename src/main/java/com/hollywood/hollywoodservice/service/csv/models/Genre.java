package com.hollywood.hollywoodservice.service.csv.models;

import lombok.AllArgsConstructor;
import lombok.Data;


//This entity represents a nested JSON object in the MoviesMetadata class
@Data
@AllArgsConstructor
public class Genre {
    private int id;
    private String name;
}

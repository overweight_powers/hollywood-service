package com.hollywood.hollywoodservice.service.csv;

import com.hollywood.hollywoodservice.config.exceptions.HollywoodException;
import com.hollywood.hollywoodservice.service.csv.models.*;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.stream.Stream;

@Service
@Slf4j
public class CsvService {

    private static final String CSV_CONTEXT_PATH = "src/main/resources/the-movies-dataset/";
    public static final String MOVIES_METADATA_CSV = "movies_metadata.csv";


    public Stream getMovieDataStream(String csvPath, Class<?> cls) {

        try {
           log.info("Fetching getMovieMetaDataStream");

            return new CsvToBeanBuilder(new FileReader(CSV_CONTEXT_PATH + csvPath))
                    .withOrderedResults(false)
                    .withThrowExceptions(false)                           // Slightly faster due to unordered results
                    .withType(cls).build().stream();

        } catch (IOException e) {
            throw new HollywoodException(HttpStatus.INTERNAL_SERVER_ERROR, "IO Exception while parsing CSV Movie MetaData", e);
        }

    }



}

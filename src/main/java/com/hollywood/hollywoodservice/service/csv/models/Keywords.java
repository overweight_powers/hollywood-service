package com.hollywood.hollywoodservice.service.csv.models;

import com.google.gson.Gson;
import com.opencsv.bean.AbstractCsvConverter;
import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Keywords  {

    @CsvBindByName
    private int id;

    @CsvBindByName
    private String keywords;

}

//-----NESTED OBJECTS-------
//record Keyword (int id, String name) {}

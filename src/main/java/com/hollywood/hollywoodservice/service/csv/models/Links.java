package com.hollywood.hollywoodservice.service.csv.models;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Links {

    @CsvBindByName
    private int movieId;

    @CsvBindByName
    private int imdbId;

    @CsvBindByName
    private int tmdbId;

}

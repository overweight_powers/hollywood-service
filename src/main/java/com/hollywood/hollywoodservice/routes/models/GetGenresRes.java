package com.hollywood.hollywoodservice.routes.models;

import com.hollywood.hollywoodservice.service.csv.models.MovieMetadata;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class GetGenresRes {

    @Schema(
            description = "Sorted genre list by popularity",
            type = "object")
    private List<String> genreList;

    @Schema(
            description = "The dataset used to generate the list of genres",
            type = "object")
    private List<MovieMetadata> dataSource;
}

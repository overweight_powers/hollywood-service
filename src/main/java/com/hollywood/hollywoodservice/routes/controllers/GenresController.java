package com.hollywood.hollywoodservice.routes.controllers;

import com.hollywood.hollywoodservice.api.GenresAPI;
import com.hollywood.hollywoodservice.routes.models.GetGenresRes;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@Tag(name = "Genres")
@RequestMapping(path = "/genres")
@AllArgsConstructor
public class GenresController {

    GenresAPI genresAPI;

    @Operation(summary = "Returns a sorted list of the most popular genres to the least popular by year.")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetGenresRes> getGenres(@RequestParam(name = "year") int year) {
        log.info("Fetching list of most popular genres for year " + year);

        return ResponseEntity.ok(genresAPI.getMostPopularGenres(year));
    }


}

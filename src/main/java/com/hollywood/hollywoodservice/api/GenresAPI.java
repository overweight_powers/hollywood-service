package com.hollywood.hollywoodservice.api;


import com.hollywood.hollywoodservice.routes.models.GetGenresRes;
import com.hollywood.hollywoodservice.service.csv.models.MovieMetadata;
import com.hollywood.hollywoodservice.service.genres.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class GenresAPI {

    private final GenreService genreService;

    @Autowired
    public GenresAPI(GenreService genreService) {
        this.genreService = genreService;
    }

    public GetGenresRes getMostPopularGenres(int year) {
        GetGenresRes res = new GetGenresRes();

        List<MovieMetadata> popularMovies = genreService.getPopularMovieData(year);
        Map<String, Long> genreCount = genreService.getGenreCount(popularMovies);

        List<String> popularGenres = new ArrayList<>(genreCount.keySet());
        popularGenres.sort((genreOne, genreTwo) -> Long.compare(genreCount.get(genreTwo), genreCount.get(genreOne)));

        res.setGenreList(popularGenres);
        res.setDataSource(popularMovies);

        return res;

    }

}

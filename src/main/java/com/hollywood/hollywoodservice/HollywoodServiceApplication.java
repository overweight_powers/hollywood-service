package com.hollywood.hollywoodservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HollywoodServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HollywoodServiceApplication.class, args);
	}

}
